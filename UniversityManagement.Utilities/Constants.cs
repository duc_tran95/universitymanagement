﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniversityManagement.Utilities
{
    public static class Constants
    {
        public readonly static int DefaultPage = 1;
        public readonly static int DefaultPageSize = 10;
    }
}
